﻿public class ModConfig
{
    public static ModConfig Loaded { get; set; } = new ModConfig();
    public bool flotsamEnabled { get; set; } = true;
    public bool fishingEntities { get; set; } = true;
    public bool anomalies { get; set; } = true;
}
