﻿using System;
using System.Collections.Generic;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.API.Util;

public class EntityFishingHook : Entity
{
    public SimpleParticleProperties waterParticles = new SimpleParticleProperties(
                    4, 8,
                    ColorUtil.ColorFromRgba(170, 150, 50, 70),
                    new Vec3d(),
                    new Vec3d(),
                    new Vec3f(-0.5f, 10f, -0.5f),
                    new Vec3f(0.5f, 5f, 0.5f),
                    0.2f,
                    2.5f,
                    0.5f,
                    0.75f,
                    EnumParticleModel.Quad
                );

    int retrievalTicks = 0;
    public bool beingRetrieved = false;

    //Entity which casted the hook
    public EntityPlayer castedBy;

    //Bait used to cast
    public ItemStack bait;

    //Pole power
    public int polePower;
    public double reelSpeed;

    public ICoreServerAPI sapi;
    public IServerNetworkChannel serverChannel;
    Random rand = new Random();

    public bool spawnParticles = false;
    public int particleColor = 0;
    public string waterColor = "blue";

    public override void Initialize(EntityProperties properties, ICoreAPI api, long InChunkIndex3d)
    {
        if (api.World.Side == EnumAppSide.Server)
        {
            sapi = api as ICoreServerAPI;
            serverChannel = (IServerNetworkChannel)api.Network.GetChannel("fishingchannel");
        }
        base.Initialize(properties, api, InChunkIndex3d);
    }

    public override void OnGameTick(float dt)
    {
        base.OnGameTick(dt);

        if (World.Side == EnumAppSide.Client && spawnParticles)
        {
            waterParticles.Color = particleColor;
            waterParticles.MinPos = Pos.XYZ.AddCopy(-0.5, 0, -0.5);
            waterParticles.AddPos.Set(1, 0, 1);
            World.SpawnParticles(waterParticles);
            spawnParticles = false;
        }

        if (World.Side == EnumAppSide.Server)
        {
            //If player does not exist or the distance to the player is > 50 blocks, die
            if (castedBy == null || !castedBy.Alive || castedBy.ServerPos.DistanceTo(ServerPos) > 50 || bait == null || castedBy.Player.ClientId == 0)
            {
                ReturnBait();
                Die();
                return;
            }

            //Water logic
            if (!beingRetrieved && Swimming) WaterLogic();

            if (beingRetrieved)
            {
                //If being retrieved for more than 60 ticks, die
                retrievalTicks++;
                if (retrievalTicks > 100)
                {
                    ReturnBait();
                    Die();
                    return;
                }

                //Spawn rewards once and move them to the bobber
                if (!rewardsSpawned && biting == true)
                {
                    if (Code.Path.Contains("jonaslure"))
                    {
                        if (CalculateFishingPower() > 100 && ModConfig.Loaded.anomalies == true) spawnWorm(); else consumeBait = false;
                        rewardsSpawned = true;
                    }
                    else
                    {
                        //Get block the hook is sitting on
                        SpawnRewards(castedBy, sapi.World.BlockAccessor.GetBlock(ServerPos.AsBlockPos).Code.ToString());
                    }
                }
                //Entity hooking
                if (!rewardsSpawned && biting == false && ModConfig.Loaded.fishingEntities == true)
                {
                    rewardStack = sapi.World.GetNearestEntity(ServerPos.XYZ, 2, 2);
                    if (rewardStack?.Code.FirstCodePart() == "fishinghook" || rewardStack == castedBy)
                    {
                        rewardStack = null;
                    }
                    rewardsSpawned = true;
                    consumeBait = false;
                }
                if (rewardStack != null)
                {
                    rewardStack.ServerPos = ServerPos;
                }

                Vec3d t = castedBy.ServerPos.XYZ - ServerPos.XYZ;
                t.Y += 2;
                t.Normalize();
                t *= reelSpeed;

                //Set motion towards the player and remove the collision
                ServerPos.Motion.Set(t.X, t.Y, t.Z);
                SetCollisionBox(0, 0);
                if (ServerPos.DistanceTo(castedBy.ServerPos.Copy().Add(0, 2, 0)) < 1)
                {
                    ReturnBait();
                    Die();
                }
            }
        }
    }

    //Boss
    public void spawnWorm()
    {
        EntityProperties type = sapi.World.GetEntityType(new AssetLocation("fishing:leviathanhead"));
        EntityLeviathanHead head = (EntityLeviathanHead)sapi.ClassRegistry.CreateEntity(type);

        head.ServerPos.SetPos(ServerPos.XYZ.Add(0, -30, 0));
        head.ServerPos.SetFrom(head.ServerPos);
        head.World = World;

        World.SpawnEntity(head);
    }

    //Water logic
    int ticksToBite = -1;
    int ticksToRetrieve = 0;
    bool biting = false;
    public void WaterLogic()
    {
        //Calculate the next amount of ticks to the next bite, static for now since I don't see what would affect this, maybe special hooks?
        //Between 10 and 30 seconds for a fish bite
        if (ticksToBite < 0)
        {
            ticksToBite = rand.Next(300, 900);
        }
        ticksToBite--;
        if (ticksToBite < 0)
        {
            ticksToRetrieve = 120; //120 ticks, about 4 seconds, after biting for retrieval
            waterColor = sapi.World.BlockAccessor.GetBlock(ServerPos.AsBlockPos).Code.ToString().Contains("water") ? "blue" : "red";
        }
        if (ticksToRetrieve > 0)
        {
            if (ticksToRetrieve % 30 == 0)
            {
                ServerPos.Y -= 0.4;
                serverChannel.BroadcastPacket(new FishingSoundMessage() //Dip under and play a sound every second
                {
                    type = 3,
                    entityId = EntityId
                });

                serverChannel.BroadcastPacket(new ParticlesMessage()
                {
                    entityId = EntityId,
                    color = waterColor == "blue" ? ColorUtil.ColorFromRgba(170, 150, 50, 70) : ColorUtil.ColorFromRgba(0, 130, 255, 70) //Blue : Red
                });
            }
            ticksToRetrieve--;
            biting = true;
        }
        else
        {
            biting = false;
        }
    }

    //REWARDS
    Entity rewardStack;
    bool rewardsSpawned = false;
    bool consumeBait = true;

    //Check liquid amount, type, and roll rewards in here
    public void SpawnRewards(Entity byEntity, string waterType)
    {
        //Generate loot
        LootObject loot = GenerateRewards(waterType);
        AssetLocation newAsset = new AssetLocation(loot.Name);

        //Check if the stack exists as an item, if not spawn a block
        ItemStack tempStack = null;
        if (byEntity.World.GetItem(newAsset) != null)
        {
            tempStack = new ItemStack(byEntity.World.GetItem(newAsset));
        }
        else
        {
            tempStack = new ItemStack(byEntity.World.GetBlock(newAsset));
        }

        //If a variant exists add it
        if (loot.Variant != null)
        {
            tempStack.Attributes.SetString("type", loot.Variant);
        }

        //Spawn an item in the world
        rewardStack = byEntity.World.SpawnItemEntity(tempStack, new Vec3d(ServerPos.X, ServerPos.Y, ServerPos.Z));
        rewardsSpawned = true;
    }

    public LootObject GenerateRewards(string waterType)
    {
        //Generate types
        FishingMod mod = (FishingMod)Api.ModLoader.GetModSystem("FishingMod");
        Dictionary<string, double> typeMultiplier = GenerateTypes(sapi.World, ServerPos.XYZ, CalculateFishingPower());

        //Select an item
        if (waterType.Contains("salt"))
        {
            return mod.saltWaterPool.SelectRandomItem(typeMultiplier);
        }
        if (waterType.Contains("boiling"))
        {
            return mod.boilingWaterPool.SelectRandomItem(typeMultiplier);
        }
        if (waterType.Contains("lava"))
        {
            return mod.lavaPool.SelectRandomItem(typeMultiplier);
        }

        //Default
        return mod.regularWaterPool.SelectRandomItem(typeMultiplier);
    }

    public override bool ApplyGravity
    {
        get 
        {   
            return !beingRetrieved;
        }
    }

    public override float MaterialDensity
    {
        get { return 800f; }
    }

    public override double SwimmingOffsetY
    {
        get { return 0.45; }
    }

    public override bool CanCollect(Entity byEntity)
    {
        return false;
    }

    public override bool ShouldReceiveDamage(DamageSource damageSource, float damage)
    {
        return false;
    }

    //Generates multipliers on the current location of the bobber
    public Dictionary<string, double> GenerateTypes(IWorldAccessor world, Vec3d pos, int fishingPower)
    {
        Dictionary<string, double> typeMultiplier = new Dictionary<string, double>();

        string[] disabledTypes = { "cannibal", "carnivore" }; //These require baits
        string type = bait.Collectible.Code.ToString();

        switch (type)
        {
            case "fishing:bait-net":
                {
                    addMultiplier(typeMultiplier, "inanimate", 5);
                    break;
                }
            case "fishing:catch-mackerel-fresh":
                {
                    addMultiplier(typeMultiplier, "cannibal", 1);
                    addMultiplier(typeMultiplier, "carnivore", 1);
                    addMultiplier(typeMultiplier, "mackerel", 0);
                    break;
                }
            case "fishing:bait-chum":
                {
                    addMultiplier(typeMultiplier, "carnivore", 5);
                    break;
                }
        }

        for (int i = 0; i < disabledTypes.Length; i++)
        {
            if (!typeMultiplier.ContainsKey(disabledTypes[i])) {
                typeMultiplier.Add(disabledTypes[i], 0);
            }
        }

        //No junk at 50 fishing power
        typeMultiplier.Add("junk", Math.Max(1 - (fishingPower / 50), 0));

        //Multiplier of 1 to rare items at 50
        typeMultiplier.Add("rare", Math.Max((fishingPower / 50), 0));

        //legendary items start appearing at 50, multiplier of 1 at 100
        typeMultiplier.Add("legendary", Math.Max((fishingPower / 50) - 1, 0));

        //Cave fish start appearing 20 below sea level, increases to 1
        typeMultiplier.Add("cave", Math.Max(1 - (pos.Y / (world.SeaLevel - 20)), 0));

        //Temperature at sea level
        ClimateCondition climate = world.BlockAccessor.GetClimateAt(new BlockPos((int)pos.X, world.SeaLevel, (int)pos.Z));
        float temperature = climate.Temperature;
        float rainfall = climate.WorldgenRainfall;

        //Cold fish can appear at -20, hot at 30
        typeMultiplier.Add("cold", temperature < -5 ? 1 : 0);
        typeMultiplier.Add("hot", temperature > 30 ? 1 : 0);

        //Rainfall values might need to be tweaked
        typeMultiplier.Add("desert", rainfall < 0.2f ? 1 : 0);
        typeMultiplier.Add("jungle", rainfall > 0.7f ? 1 : 0);

        return typeMultiplier;
    }

    public void addMultiplier(Dictionary<string, double> typeMultiplier, string type, double multiplier)
    {
        if (typeMultiplier.ContainsKey(type))
        {
            double newMultiplier = multiplier * typeMultiplier.Get(type);
            typeMultiplier.Remove(type);
            typeMultiplier.Add(type, newMultiplier);
        } else
        {
            typeMultiplier.Add(type, multiplier);
        }
    }

    //Return the bait if the chance succeeds or the rewards weren't spawned
    public void ReturnBait()
    {
        if (bait == null || castedBy == null) return;
        if (rand.NextDouble() > bait.Collectible.Attributes["breakChance"].AsDouble() || rewardsSpawned == false || consumeBait == false)
        {
            if (!castedBy.TryGiveItemStack(bait))
            {
                castedBy.World.SpawnItemEntity(bait, castedBy.ServerPos.XYZ);
            }
        }
    }

    int fluidBlocks;

    public int CalculateFishingPower()
    {
        fluidBlocks = 0;
        sapi.World.BlockAccessor.SearchFluidBlocks(ServerPos.AsBlockPos.AddCopy(-10, 0, -10), ServerPos.AsBlockPos.AddCopy(10, -10, 10), OnBlock); //Calculates it up to 10 blocks down, 10 xz radius

        if (fluidBlocks < 500)
        {
            return (-500 + fluidBlocks) + polePower + bait.Collectible.Attributes["baitPower"].AsInt(); //-500 at 1 block, going up to +0 at 500 blocks
        }
        else
        {
            return (fluidBlocks / 80) + polePower + bait.Collectible.Attributes["baitPower"].AsInt(); //+50 at max blocks
        }
    }

    public bool OnBlock(Block forblock, BlockPos atpos)
    {
        if (forblock.FirstCodePart().Contains("water"))
        {
            fluidBlocks++;
        }
        return true;
    }
}
